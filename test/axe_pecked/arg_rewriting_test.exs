alias AxePeckedTest.ArgRewritingTest.Target
alias AxePeckedTest.ArgRewritingTest.Injection
defmodule AxePeckedTest.ArgRewritingTest do
    use ExUnit.Case, async: true
    test "args can be re-written" do
        result = Target.test_method_1(1)
        assert result == 2
    end



    defmodule Target do
        use AxePecked, inject: Injection,
                       pre_returns_new_args: true
        def test_method_1 arg do
            arg
        end
    end

    defmodule Injection do
        
        def pre %{method: :test_method_1, args: [arg]} do
            [arg + 1]
        end
        def pre(_others), do: :ok

        def post(_any), do: :ok
    end

end