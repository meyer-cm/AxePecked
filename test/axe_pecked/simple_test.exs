alias AxePeckedTest.SimpleTest.Target
alias AxePeckedTest.SimpleTest.Injection
defmodule AxePeckedTest.SimpleTest do
    use ExUnit.Case, async: true
    test "method with no args works correctly" do
        Target.test_method_1
        assert_received {:before_test_method_1, []}
        assert_received {:after_test_method_1, :test_method_1_ok}
    end
        
    test "method with 1 arg works correctly" do
        Target.test_method_2(5)
        assert_received {:before_test_method_2, [5]}
        assert_received {:after_test_method_2, 6}
    end

    test "method with no args and no paren works correctly" do
        Target.test_method_3
        assert_received {:before_test_method_3, []}
        assert_received {:after_test_method_3, :test_method_3_ok}
    end

    test "aspect does not change return value" do
        result = Target.test_method_1
        assert result == :test_method_1_ok
    end


    defmodule Target do
        use AxePecked, inject: Injection
        def test_method_1() do
            :test_method_1_ok
        end
        def test_method_2(arg) do
            arg + 1
        end
        def test_method_3 do
            :test_method_3_ok
        end
    end

    defmodule Injection do
        
        def pre %{method: :test_method_1, args: args} do
            send self, {:before_test_method_1, args}
        end
        def pre %{method: :test_method_2, args: args} do
            send self, {:before_test_method_2, args}
        end
        def pre %{method: :test_method_3, args: args} do
            send self, {:before_test_method_3, args}
        end
        def pre unexpected do
            send self, {:pre_unexpected, {unexpected}}
        end

        def post %{method: :test_method_1, result: result} do
            send self, {:after_test_method_1, result}
        end

        def post %{method: :test_method_2, result: result} do
            send self, {:after_test_method_2, result}
        end

        def post %{method: :test_method_3, result: result} do
            send self, {:after_test_method_3, result}
        end
        def post unexpected do
            send self, {:post_unexpected, {unexpected}}
        end
    end
end