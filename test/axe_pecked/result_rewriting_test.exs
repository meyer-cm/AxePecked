alias AxePeckedTest.ResultRewritingTest.Target
alias AxePeckedTest.ResultRewritingTest.Injection
defmodule AxePeckedTest.ResultRewritingTest do
    use ExUnit.Case, async: true
    test "result can be re-written" do
        result = Target.test_method_1(1)
        assert result == 2
    end



    defmodule Target do
        use AxePecked, inject: Injection,
                       post_returns_new_result: true
        def test_method_1 arg do
            arg
        end
    end

    defmodule Injection do
        
        def pre(%{method: _everything, args: args}), do: args

        def post %{method: :test_method_1, result: result} do
            result + 1
        end
        def post(%{method: _other, result: result}), do: result

    end

end