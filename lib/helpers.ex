defmodule AxePecked.Helpers do
    @doc """
    Simple function allows use of string concatenation during pipelining

        iex> concat("hello ", "world")
        "hello world"

        iex> "Hey" 
        ...> |> concat(" ")
        ...> |> concat("dude")
        "Hey dude"
    """
    def concat(str1, str2), do: str1 <> str2

    @doc """
    updates a value based on a map of replacements.  intended for use in pipelining and Enum.map.  

        iex> replace(:some_atom, %{some_atom: "replacement"})
        "replacement"

        iex> replace(:some_atom, %{some_other_atom: "replacement"}) # <= no match
        :some_atom

    makes sense to use a large map for larger replacements:

        iex> replacements = %{1 => "a", 2 => 5, 4 => "d"}
        ...> collection = [1, 2, 3, 4]
        ...> |> Enum.map &replace(&1, replacements)
        ["a", 5, 3, "d"]
    """
    def replace(orig, map) do
        case Dict.has_key?(map, orig) do
            true -> map[orig]
            false -> orig
        end
    end 

    def inner_call({orig_symbol, args, context}) do
        {orig_symbol |> inner_symbol, args, context}
    end

    def inner_symbol(orig_symbol) do
        orig_symbol 
        |> Atom.to_string
        |> concat("_original")
        |> String.to_atom
    end

    def execute_aspected(module, function, actual_args, opts) do
        info = %{args: actual_args, method: function}
        new_args = apply opts[:inject], :pre, [info]
        if opts[:pre_returns_new_args] do
            actual_args = new_args
        end
        new_symbol = function |> inner_symbol
        result = apply(module, new_symbol, actual_args)
        new_result = apply opts[:inject], :post, [Dict.put_new(info, :result, result)]
        if opts[:post_returns_new_result] do
            result = new_result
        end
        result
    end
end