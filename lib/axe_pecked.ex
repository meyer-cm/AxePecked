alias AxePecked.Helpers
defmodule AxePecked do
    defmacro def(original_call, do: contents) do
        {orig_symbol, _context, args} = original_call
        args = Helpers.replace(args, %{nil => []})

        new_call = original_call |> Helpers.inner_call
        quote do
            Kernel.def(unquote(new_call), do: unquote(contents))
            Kernel.def unquote(original_call) do
                Helpers.execute_aspected(__MODULE__, unquote(orig_symbol), 
                    unquote(args), @axepecked_opts)
            end
        end
    end

    defmacro __using__(opts) do
        quote do
            import Kernel, except: [def: 2]
            import AxePecked, only: [def: 2]
            @axepecked_opts unquote(opts)
        end
    end
end

