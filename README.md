AxePecked
=========

AxePecked is a simple approach to Aspect Oriented Programming (AOP) in Elixir. 
With AOP, cross-cutting concerns (like logging) can be consolidated rather than 
being spread across the codebase and mixed amongst business logic.

Most of us have seen (or written) code like this:

```elixir
defmodule BusinessLogic do
    def involved_business_logic(arg_1, arg_2) do
        Logging.debug("entering involved_business_logic with args #{arg_1} and #{arg2}")
        initial_result = execute_involved_step_1(arg_1)
        secondary_result = execute_involved_step_2(arg_2)
        combined_result = combine(initial_result, secondary_result)
        Logging.info "completed involved_business_logic, result: #{combined_result}"
        combined_result
    end
end
```
This is simple code, but it's not very easy to scan, and additionally, it breaks SRP and does **two**
things: Business Logic, and Logging. Furthermore, it isn't hard to envision (or remember) a situation
where our code's intent is muddled because of the extraneous logging statements.

To separate these concerns, one approach is AOP. Using AxePecked, we create two files:

```elixir
#business_logic.ex:
defmodule BusinessLogic do
   use AxePecked, inject: BusinessLogic.Logging # <= can be any Module
   # No logging here...
   def involved_business_logic(arg_1, arg_2) do
       initial_result = execute_involved_step_1(arg_1)
       secondary_result = execute_involved_step_2(arg_2)
       combine(initial_result, secondary_result)
   end
end

# business_logic_logging.ex:
defmodule BusinessLogic.Logging do
    # No business logic here...
    def pre(%{method: :involved_business_logic, args: [arg1, arg2]}) do
        Logging.debug("entering involved_business_logic with args #{arg_1} and #{arg2}")
    end
    def post(%{method: :involved_business_logic, result: combined_result}) do
        Logging.info "completed involved_business_logic, result: #{combined_result}"
    end
end
```

The :pre method is called before each method in the original module, and passed a map of the module name and the args.  The :post method is called after each method, and passed a map of the module name, args, and result.

For those of you who love the pipeline operator and/or pattern matching, check this out:

```elixir
# wish_i_could_pipeline_here.ex
defmodule WishICouldPipelineHere do
    def not_pipelineable(input) do
        Logging.debug "starting what should have been a pipeline with: #{input}"
        result1 = op1(input)
        Logging.debug "finished op1, sent in #{input}, got back #{result1}"
        result2 = op2(result1)
        Logging.debug "finished op2, sent in #{result1}, got back #{result2}"
        result3 = op2(result2)
        Logging.debug "finished op3, sent in #{result2}, got back #{result3}"
        result3
    end
    def op1(arg), do: arg + 1
    def op2(arg), do: arg + 2
    def op3(arg), do: arg + 3
end
```

Yes, this is a silly example, but mentally substitute addition for complicated operations, and you'll see this isn't a trivial example. Not only can we not pipeline here, as our elixir blood yearns to do, but we actually have more lines of code conducting logging functions than calling out to our operational code.  Lets try again with AxePecked:

```elixir
# pipe_in_effect.ex:
defmodule PipeInEffect do
    use AxePecked, inject: PipedLogging
    def pipelineable(input), do: input |> op1 |> op2 |> op3
end

# piped_logging.ex:
defmodule PipedLogging do
    def pre(%{method: :pipelineable, args: [input]}) do
        Logging.debug "starting a proper pipeline with: #{input}"
    end
    def pre(_others), do: :ok #no need to log

    def post(%{method_name: any, args: [input], result: result}) do
        Logging.debug "finished #{any}, sent in #{input}, got back #{result}"
    end
end
```

Advanced AOP
------
The killer feature for AOP in my opinion is what's been shown above, injecting code around the entry/exit points of functions.  However, in proper AOP, you the injected code (advice, in the AOP parlance) can actually adjust the original code's arguments and return value!  In AxePecked, these capabilities must be individually enabled, as they are a source of bugs for the unwary.  By setting :pre_returns_new_args or :post_returns_new_result (named to hopefully be self explanatory!), the injection methods actually modify the original code.  Use carefully.
```elixir
defmodule Target do
    use AxePecked, inject: Injection,
                   pre_returns_new_args: true,
                   post_returns_new_result: true
    def test_method_1 arg do
        arg
    end
end

defmodule Injection do
    def pre %{method: :test_method_1, args: [arg]} do
        #ha- not so fast:
        [arg + 1000]
    end
    def post %{method: :test_method_1, result: result} do
        #well, on second thought...
        result - 1000
    end
end
```

Features
------
AOP basics:   
- hook just before and just after each method call in a module

AOP advanced: 
- change the arguments on the way into a method
- change the result on the way out of a method

Not yet implemented / TODO
------
AOP advanced: 
- allow :pre to prevent execution of aspected method

Examples:
- simple example to highlight logging use-case

Performance:
- benchmark cost of aspecting a method call
- benchmark cost to recursive/tail recursive situations
- examine stack-traces for comprehendability for both aspected methods and advice.

Testing:
- test case for recursive call
- test case for tail recursion
- more tests!

Important
------
AxePecked is still in it's infancy;  the entire API will likely shuffle as feedback comes in from around the community.

License
------
DWTFYW + Beer and Pizza
